(function () {
	window.onload = function () {
		var random = Math.floor(Math.random() * 100000000000000000);
		var frameHolder = 'frame' + random;
		var projectID = '{{ID}}';
		var div = document.createElement('div');
		div.id = frameHolder;
		currentScript.parentElement.insertBefore(div, currentScript);
		var iframeHolder = document.getElementById(frameHolder),
			iframe = document.createElement("iframe");
		var icontent = contentPage;
		iframe.src = 'data:text/html;charset=utf-8,' + encodeURIComponent(icontent);
		iframe.width = '100%';
		iframe.height = '100%';
		iframe.scrolling = 'no';
		iframe.style.backgroundColor = "transparent";
		iframe.allowTransparency = "true";
		iframe.frameBorder = 0;
		iframe.style.position = "relative";
		iframeHolder.appendChild(iframe);
		iframeHolder.style.width = options.contentWidth + 'px';

		function setPlayerHeight() {
			var aspectRatioCoef = 1.565;
			var timer = setTimeout(function go() {
				iframeHolder.style.height = iframeHolder.offsetWidth / aspectRatioCoef + 'px';
				timer = setTimeout(go, 100)
			}, 100);
		}

		setPlayerHeight();
	};

})()