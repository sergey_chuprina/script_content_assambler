'use strict';


const project = 'test_project';
const copySrc = 'test_project';





const gulp 			= require('gulp'),
$ 					= require('gulp-load-plugins')({lazy: true}),
inject 				= require('gulp-inline-source'),
panini 				= require('panini'),
minify				= require('gulp-html-minify'),
runSequence 		= require('run-sequence'),
rev 				= require('gulp-rev-append'),
fse 				= require('fs-extra'),
ftp 				= require('vinyl-ftp'),
map 				= require('vinyl-map'),
browserSync 		= require('browser-sync').create();
require('dotenv').config();

const format = "utf8";

const path = {
	src: 'src/' + project,
	dist: 'dist/' + project,
	copy: 'src/' + copySrc + '/**/*'
};

//sass
gulp.task('scss', ()=>{
	return gulp.src(path.src + '/assets/scss/style.scss')
	.pipe($.sass().on( 'error', $.notify.onError({message: "<%= error.message %>", title  : "Sass Error!"})))
	.pipe($.autoprefixer({
		browsers: ['last 4 versions'],
		cascade: true
	}))
	.pipe($.csscomb())
	.pipe(gulp.dest(path.src + '/assets/min/'));
})
////////////////////////////////
//js
gulp.task('js', () => {
	return gulp.src(path.src + '/assets/js/script.js')
	.pipe($.babel({
		presets: ['env']
	}).on( 'error', $.notify.onError({message: "<%= error.message %>", title  : "Babel Error!"})))
	.pipe($.uglify().on( 'error', $.notify.onError({message: "<%= error.message %>", title  : "JS-Uglify Error!"})))
	.pipe($.rename({
		suffix: ".min",
	}))
	.pipe(gulp.dest(path.src + '/assets/min/'));
});

gulp.task('common:js', () => {
	let createCommonJS = map(function(code, filename) {
		let page = path.src + '/assets/min/page.html';
		let pageContent = "'" + fse.readFileSync(page, format) + "'";
		let regExp = /contentPage/;
		code = code.toString();
		code = code.replace(regExp, pageContent);
		code = code.replace('{{ID}}', project);
		return code;
	});

	return gulp.src(path.src + '/common.js')
	.pipe(createCommonJS)
	.pipe(gulp.dest(path.dist + '/'))
	.pipe($.rename({
		suffix: ".js",
		extname: ".txt",
	}))
	.pipe(gulp.dest(path.dist + '/'))
	.pipe(browserSync.stream());
});
////////////////////////////////

//html
gulp.task('inline-src', ()=>{
	let pageElement = path.src + '/layouts/default-index.html';
	let pageContent = fse.readFileSync(pageElement, format);
	var regexp = {
		style: /inline\ssrc\/css\/style\.css/,
		script: /inline\ssrc\/js\/script\.js/
	};
	let newStylePath = 'inline '+path.src+'/assets/min/style.css';
	let newScriptPath = 'inline '+path.src+'/assets/min/script.min.js';
	while(pageContent.search(regexp.style) >= 0 && pageContent.search(regexp.script) >= 0){
		pageContent = pageContent.replace(regexp.style, newStylePath);
		pageContent = pageContent.replace(regexp.script, newScriptPath);
	}
	pageElement = path.src + '/layouts/default.html';
	fse.writeFile(pageElement, pageContent);
});

gulp.task('page', () => {
	gulp.src(path.src + '/pages/**/*.html')
	.pipe(panini({
		root: path.src + '/pages/',
		layouts: path.src + '/layouts/'
	}))
	.pipe(inject())
	.pipe(minify())
	.pipe(gulp.dest(path.src + '/assets/min'));
});

gulp.task('inject', () => {
	gulp.src(path.src + '/layouts/default.html')
	.pipe(inject())
	.pipe(gulp.dest(path.src + '/layouts/'));
});

gulp.task('html', () => {
	let oldTitle = /{{title}}/;

	gulp.src(path.src + '/index.html')
	.pipe($.replace(oldTitle, project))
	.pipe(gulp.dest(path.dist))
	.pipe(browserSync.stream());
});

gulp.task('revision', () => {
	gulp.src(path.dist + '/index.html')
	.pipe(rev())
	.pipe(gulp.dest(path.dist))
	.pipe(browserSync.stream());
});


/*============ Server ============*/

gulp.task('serve', function(){
	startBrowserSync(path.dist);
});

function startBrowserSync(basePaths) {
	let options = {
		port: 5000,
		ghostMode: {
			clicks: false,
			location: false,
			forms: false,
			scroll: true
		},
		injectChanges: true,
		logFileChanges: true,
		logLevel: 'debug',
		logPrefix: 'gulp-patterns',
		notify: true,
		reloadDelay: 0,
		online: false,
		server: {
			baseDir: basePaths
		},
		open: true
	};
	browserSync.init(options);
}

gulp.task('checkDist', () => {
	var distFolder = fse.readdirSync('./dist');
	var timerId = setTimeout(function run(){
		if(distFolder.indexOf(project)){
			gulp.start('serve');
			return;
		}
		timerId = setTimeout(run(), 1000);
	}, 1000);
});


/*============ Default ============*/

gulp.task('watch', () => {
	gulp.watch(path.src + '/layouts/default-index.html', ['inline-src']);
	gulp.watch(path.src + '/assets/js/script.js', ['js']);
	gulp.watch(path.src + "/assets/scss/**/*.scss", ['scss']);
	gulp.watch([path.src + "/assets/min/style.css", path.src + "/assets/min/script.min.js"], ['inject']);
	gulp.watch([path.src + "/layouts/default.html", path.src + "/pages/page.html"], ['page']);
	gulp.watch([path.src + "/common.js", path.src + "/assets/min/page.html"], ['common:js', 'revision']);
	gulp.watch(path.src + "/index.html", ['html']);
});


gulp.task('dist', () => {
	runSequence('inline-src', 'js', 'scss', 'page', 'common:js', 'html', 'serve', 'watch');
});

gulp.task('clean', () => {
	return gulp.src('./dist/*')
	.pipe($.clean({force: true}));
});

gulp.task('start', ['clean', 'dist']);

gulp.task('copy', () => {
	gulp.src(path.copy)
	.pipe(gulp.dest(path.src));
});

gulp.task('default', () => {
	try{
		fse.readFileSync(path.src + '/index.html', format);
	} catch(err){
		console.log(new Error('You must create a project at the first with "gulp copy"!'));
		process.exit();
	}
	gulp.start('start');
});